# Cómo empezar con devilbox Craft CMS

Esta página te ayudará a empezar con Devilbox Craft CMS. ¡Estarás en marcha en un santiamén!

## 1. Proceso de instalación:
El proceso de instalación no es nada complicado, no tengas miedo joven hechicero 🧙‍♂ a darle!!🤘
**Antes de comenzar  debemos revisar que los servicios de nginx, mysql, postgresql, memcached.
estén detenidos, si no lo están podemos usar el comando: `sudo systemctl start stop (nombre del servicio).service`**

 - Debes identificar una ruta de tu preferencia 🕵, puede ser:
	 - Linux o Mac os:   `/home/$user/`
	 - Windows: `c:/ `
- Una vez elegida la ruta debes clonar el repositorio de Devilbox 🧑‍💻:
	- Linux o Mac os:  `cd /home/$user/ && git clone https://github.com/cytopia/devilbox`
	- Windows: Debes contar con la herramienta de [Git for Windows](https://git-scm.com/downloads) una vez instalada debes entrar en la ruta antes seleccionada y clonar el proyecto `git clone https://github.com/cytopia/devilbox`

## 2. Configuración:
Vez si puede animo 😁🎉!!, para el proceso de configuración debes ingresar al directorio de devilbox y hacer lo siguiente:

 - Linux o Mac os:  `cp env-example .env`
 - Windows: Debes generar una copia del env-example.
 -  Editar el nuevo archivo **.env** y especificar las versiones de los servicios que vas a utilizar y asignar un password a servicios como mysql o postgresql un ejemplo:
    - ![Seleccionado la version de Mysql](https://i.ibb.co/bNd4s0V/Screenshot-20210909-133109.png)
    - ![Seleccionado la version de Mysql](https://i.ibb.co/sHpyFtB/Screenshot-20210909-133854.png)

## 3. Integración con Craft CMS:
Listo ya casi terminamos 🧘‍♂, para la integración con Craf CMS, debes tener en cuenta que la ruta por defecto para el alojamiento de los proyectos esta en `/devilbox/data/www`  aquí debes crear un directorio con el nombre del domino que se le va asignar al sitio por ejemplo: **example.loc** el directorio debe tener el nombre de **example/**  una vez creado debes ingresar a este y debes clonar el repositorio.
 - Una clonado el repo debes crear un enlace simbólico de la carpeta web del proyecto `ln -s example/web/ htdocs`
 - Debes editar el archivo host:
	 - Linux o Mac os: `sudo nano /etc/hosts` 
	 - windows: `C:\Windows\ System32\drivers\etc\hosts`
	 ![Hosts](https://i.ibb.co/zhdSRJp/Screenshot-20210909-135848.png)
- Ejecutar:  `docker-compose up`

## 4. Listo🎉😁🎉🧘‍♂!!
Todo lo bueno tiene que terminar, una vez realizado todo lo anterior  debes tener el panel de devilbox y en http://localhost/vhosts.php tu sitio debe estar en un estatus ok. 
para el proceso instalar craft y realizar todo el proceso de la documentación existente en TBI debes ejecutar en la ruta de devilbox `./shell.sh` esto abrirá una shell dentro del contenedor y podrás entrar al directorio de  tu proyecto y ejecutar los comando de craft y documentación que esta en TBI, `Nota: funciona todo lo del directorio de scripts.`

![Scripts](https://i.ibb.co/VxTQS2t/Screenshot-20210909-134117.png)
![Scripts](https://i.ibb.co/dkmgd8p/Screenshot-20210909-134415.png)
![Panels](https://i.ibb.co/DV5fjTb/Screenshot-20210909-134620.png)
![Panels](https://i.ibb.co/gZB3fwR/Screenshot-20210909-134802.png)

## Enlaces

- https://devilbox.readthedocs.io/en/latest/getting-started/prerequisites.html
- https://devilbox.readthedocs.io/en/latest/examples/setup-craftcms.html
